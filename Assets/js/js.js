class RSP{
    constructor(){
        this.beginTitle = document.querySelector('.awal')
        this.centerSide = document.querySelector('.akhir')
        this.statusTitle = document.querySelector('.hasil>h1')
        this.statusTitle1= document.querySelector('.keterangan>h5')
        this.user_sc= document.getElementById('user_sc')
        this.comp_sc=document.getElementById('comp_sc')
        this.scoreUser=0;
        this.scoreComp=0;
    }

    hasil(userCh, computerChoice){
        this.beginTitle.classList.add('sembunyi')
        this.centerSide.classList.remove('sembunyi')
        const user= userCh
        const computer = computerChoice
        console.log('user =>'+user)
        console.log('computer =>'+computer)
        if(user==="batu" && computer==="gunting"||user==="kertas" && computer==="batu"||user==="gunting" && computer==="kertas"){
            this.scoreUser++
            this.user_sc.innerHTML=this.scoreUser
            this.comp_sc.innerHTML=this.scoreComp
            this.statusTitle.innerHTML = `PLAYER 1 <br> WIN`
            this.statusTitle1.innerHTML = `${user} : ${computer}`
            console.log('player menang')
        }
        else if(user==="batu" && computer==="kertas"||user==="kertas" && computer==="gunting"||user==="gunting" && computer==="batu"){
            this.scoreComp++
            this.user_sc.innerHTML=this.scoreUser
            this.comp_sc.innerHTML=this.scoreComp
            this.statusTitle.innerHTML = `COM <br> WIN`
            this.statusTitle1.innerHTML = `${user} : ${computer}`
            console.log('computer menang')
        }
        else{console.log('draw')
            this.statusTitle.innerHTML = 'DRAW'
            this.statusTitle1.innerHTML = `${user} : ${computer}`
            console.log('Seri')
        }
    }
}

const Komp = Base => class extends Base{
    getComputerChoice(){
        const choice = ['batu','gunting','kertas']
        const randomNumber = Math.floor(Math.random()* 3)
        return choice[randomNumber]
    }
    
}

class Pengguna extends Komp(RSP){
    constructor(){
        super();
        this.user =document.querySelectorAll('.user')
        this.refreshButton = document.querySelector('.refresh>img')
        this.comp = document.querySelectorAll('.comp')

        this.user.forEach(u => {
            u.addEventListener('click',this.game.bind(this))
        });

        this.refreshButton.addEventListener('click',this.reset.bind())
    }

    game=(userChoice)=>{
        const computerChoice = this.getComputerChoice()
        const userCh = userChoice.target.alt
        this.hasil(userCh,computerChoice)
    }

    reset=()=>{
        this.scoreUser=0;
        this.scoreComp=0;
        this.user_sc.innerHTML=0
        this.comp_sc.innerHTML=0
        this.beginTitle.classList.remove('sembunyi')
        this.centerSide.classList.add('sembunyi')
        this.statusTitle1.innerHTML=''
    }
}

const GOO = new Pengguna();