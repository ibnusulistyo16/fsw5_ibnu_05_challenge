const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const fs = require('fs');
const app= express();
const router = require('./router')
const posts= require('./masterdata/user_data.json')

let PORT= 8000;
app.set('view engine','ejs');

app.use(morgan('dev'))
app.use(express.json())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use(express.static(__dirname + '/'));
app.use(router)

app.get('/api/posts',(req,res)=>{
    res.status(200).json(posts)
})

app.get('/api/posts/:id',(req,res)=>{
    const post = posts.find(singlePost=>singlePost.id=== parseInt(req.params.id,10))
    res.status(200).json(post)
})

app.post('/api/posts',(req,res)=>{
    const username = req.body.username;
    const lastIndex = posts.length-1;
    const id = posts[lastIndex].id+1;

    const newPost= {
        id:id,
        username:username
    }
    posts.push(newPost)

    fs.writeFile('./masterdata/user_data.json', JSON.stringify(posts), (err)=>{
        if(err){
           res.status(400).json({
               message:"Something wrong when post the data",
           }) 
        }
        else{
            res.status(200).json({
                message:"Succesful",
            })
        }
    })

    // res.status(200).json(newPost)
    // res.redirect('/rps');
    res.redirect('/rps?username='+username);
   
})


app.put('/api/posts/:id',(req,res)=>{
    let post = posts.find(x=> x.id === parseInt(req.params.id,10))
    let updatePost={};
    const params = {
        username: req.body.username
    }

    updatePost ={
        ...post,
        ...params
    }

    //cara 1
    posts=posts.map((x)=>(x.id === updatePost.id?updatePost:x))
    res.status(200).json(posts)
})

app.delete('/api/posts/:id',(req,res)=>{
    posts =posts.filter((x)=>x.id !== parseInt(req.params.id,10))
    res.status(200).json(posts)
})

app.listen(PORT, ()=>{
    console.log('Server Listening in port'+PORT)
})