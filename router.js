const express = require('express');
const { reset } = require('nodemon');
const router = express.Router();
const app = express();

app.set('view engine','ejs');
app.use(express.json())
router.use(function timeLog(req,res,next){
    console.log('Time : ', Date.now())
    next()
})

router.get('/', (req,res)=>{
    res.render('index')
})

router.get('/rps',(req,res)=>{
    const username=req.query.username;
    res.render('rps_game',{username})
})

// router.post('/rps',(req,res)=>{
//     const username=req.body.username;
//     res.render('rps_game', {username})
// })

router.get('/login',(req,res)=>{
    res.render('login')
})

router.get('/add',(req,res)=>{
    res.render('add')
})

router.get('/reset',(req,res)=>{
    res.render('reset')
})

module.exports = router;